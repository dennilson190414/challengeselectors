package com.challenge.munoz;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;

public class MunozAutomatedTest {
	
	private WebDriver driver;
	By pageLocator = By.xpath("//*[contains(concat(\" \", normalize-space(@class), \" \"), \"entry-title \")]" );
	By YourName = By.name("user-submitted-name");
	By YourEmail = By.cssSelector("input[name=\"user-submitted-email\"]");
	By YourTitle = By.name("user-submitted-title");
	By YourUrl = By.name("user-submitted-url[]");
	By YourContent = By.name("user-submitted-content");
	By YourSubmit = By.id("user-submitted-post");
	
	@Before
	public void setUp() throws Exception {
		System.setProperty("webdriver.chrome.driver", "./src/test/resources/driver/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://apprater.net/add/");
	}

	@After
	public void tearDown() throws Exception {
		
	}

	@Test
	public void test()  {
		if (driver.findElement(pageLocator).isDisplayed()) {
			driver.findElement(YourName).sendKeys("dennilson Rafael");
			driver.findElement(YourEmail).sendKeys("dennilson@otmail.com");
			driver.findElement(YourTitle).sendKeys("Dr Challenge");
			driver.findElement(YourUrl).sendKeys("https://apprater.net/add/");
			driver.findElement(YourContent).sendKeys("Hola mi nombre Dennilson Rafael Mu�oz Rojas");
			
			
		}else {
		System.out.println("Error");
	}
		}

}
